package pagination

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"

	"bitbucket.org/sergsoloviev/db"
)

type Pagination interface {
	All(params ...interface{}) string
	Counter() string
	Factory(*sql.Rows) ([]interface{}, error)
}

type Objects struct {
	Type   Pagination
	Objs   []interface{}
	Limit  int
	Offset int
	Page   int
	Count  int
	Pages  int
	Prev   interface{}
	Next   interface{}
}

var (
	ListLimit = 10
)

func (o *Objects) Init() {
	o.Objs = make([]interface{}, 0)
	o.Limit = ListLimit
	o.Offset = 0
	o.Page = 1
}

func (o *Objects) Parse(r *http.Request) {
	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err == nil {
		o.Page = page
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err == nil && limit != 0 {
		o.Limit = limit
	}
	if o.Page < 1 {
		o.Page = 1
	}
	o.Offset = (o.Page - 1) * o.Limit
}

func (o *Objects) PrevNextLinks() error {
	if o.Page > 1 {
		if o.Limit != ListLimit {
			o.Prev = fmt.Sprintf("?page=%d&limit=%d", o.Page-1, o.Limit)
		} else {
			o.Prev = fmt.Sprintf("?page=%d", o.Page-1)
		}
	}
	if o.Page < o.Pages {
		if o.Limit != ListLimit {
			o.Next = fmt.Sprintf("?page=%d&limit=%d", o.Page+1, o.Limit)
		} else {
			o.Next = fmt.Sprintf("?page=%d", o.Page+1)
		}
	}
	return nil
}

func (o *Objects) All(params ...interface{}) error {
	err := o.Counter()
	if err != nil {
		return err
	}

	var q string
	if len(params) > 0 {
		q = o.Type.All(params)
	} else {
		q = o.Type.All()
	}

	stmt, err := db.DB.Prepare(q)
	if err != nil {
		log.Println(err)
		return err
	}
	defer stmt.Close()
	rows, err := stmt.Query(o.Limit, o.Offset)
	if err != nil {
		log.Println(err)
		return err
	}
	defer rows.Close()

	o.Objs, err = o.Type.Factory(rows)
	if err != nil {
		log.Println(err)
		return err
	}
	err = o.PrevNextLinks()
	if err != nil {
		return err
	}
	return nil
}

func (o *Objects) Counter() error {
	row := db.DB.QueryRow(o.Type.Counter())
	err := row.Scan(&o.Count)
	if err != nil {
		log.Println(err)
		return err
	}
	o.Pages = int(math.Ceil(float64(o.Count) / float64(o.Limit)))
	if o.Page > o.Pages {
		o.Page = o.Pages
		o.Offset = (o.Page - 1) * o.Limit
	}
	return nil
}
